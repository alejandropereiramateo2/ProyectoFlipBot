using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{

    [SerializeField]
    public Slider sliderVolumen;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("volumenMusica"))
        {
            PlayerPrefs.SetFloat("volumenMusica", 1);
            cargarValores();
        }
        else
        {
            cargarValores();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CambiarVolumen()
    {
        AudioListener.volume = sliderVolumen.value;
        guardarValores();
    }

    public void guardarValores()
    {
        PlayerPrefs.SetFloat("volumenMusica", sliderVolumen.value);
    }

    public void cargarValores()
    {
        sliderVolumen.value = PlayerPrefs.GetFloat("volumenMusica");
    }
}
