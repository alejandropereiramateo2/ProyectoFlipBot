using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataPersistanceManager : MonoBehaviour
{

    public static DataPersistanceManager instance { get; private set; }

    private GameData gameData;

    private void Awake()
    {
        if (instance != null) {
            Debug.Log("Se ha encontrado mas de un controlador. Vamos a destruir el mas reciente.");
            Destroy(this.gameObject);
            return;
        }
    }

    public void NuevoJuego()
    {
        this.gameData = new GameData();
    }

    public void Continuar()
    {
        if (this.gameData == null)
        {
            Debug.Log("No se han encontrado datos. Se necesitara empezar un nuevo juego");
            return;
        }
    }

    public void GuardarJuego()
    {

    }

    private void OnApplicationQuit()
    {
        GuardarJuego();
    }

}
