using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Rotar : MonoBehaviour
{

    private Quaternion Angulo0 = Quaternion.Euler(0, 0, 0);
    private Quaternion Sumar90 = Quaternion.Euler(0, 0, 90);
    private Quaternion Sumar180 = Quaternion.Euler(0, 0, 180);
    private Quaternion Restar90 = Quaternion.Euler(0, 0, 270);

    public Quaternion AnguloActual;
    public float VelocidadRotacion = 0.2f;

    public float duracionRotacion;
    public float TiempoEspera;

    private CheckGrounded checkSuelo;

    [Header("Variables Rotacion")]
    public UnityEvent onRotacionStart;
    public bool OverlapCircle { get; set; }
    public bool rotando { get; set; }

    [Header("Audio")]
    public AudioSource audios;

    // Start is called before the first frame update
    void Start()
    {
        AnguloActual = Angulo0;

        checkSuelo = GameObject.Find("Jugador").GetComponent<CheckGrounded>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("FlechaUp")) //Sumar 180 
        {
            if (rotando)
            {
                return;
            }
            else
            {
                if (OverlapCircle)
                {
                    onRotacionStart.Invoke();

                    CambioDeAnguloMas180();

                    Time.timeScale = 0.0f;
                    StartCoroutine(Rotacion(duracionRotacion));
                }
            }
        }

        if (Input.GetButtonDown("FlechaLeft")) //Sumar 90 (Gsirar Der)
        {
            if (rotando)
            {
                return;

            }
            else
            {
                if (OverlapCircle)
                {
                    onRotacionStart.Invoke();

                    CambioDeAnguloMas90();

                    Time.timeScale = 0.0f;
                    StartCoroutine(Rotacion(duracionRotacion));
                }
            }
        }

        if (Input.GetButtonDown("FlechaRight")) //Restar 90 (Girar Izq)
        {
            if (rotando)
            {
                return;

            }
            else
            {
                if (OverlapCircle)
                {
                    onRotacionStart.Invoke();

                    CambioDeAnguloMenos90();

                    Time.timeScale = 0.0f;
                    StartCoroutine(Rotacion(duracionRotacion));

                }
            }
        }

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, AnguloActual, VelocidadRotacion);

    }

    private void FixedUpdate()
    {
        if (checkSuelo.TocandoSuelo || checkSuelo.TocandoCaja || checkSuelo.TocandoBoton)
        {

            OverlapCircle = true;
        }
        else
        {
            OverlapCircle = false;
        }
    }

    public void CambioDeAnguloMas180()
    {
        Vector3 Operacion = AnguloActual.eulerAngles + Sumar180.eulerAngles;
        AnguloActual = Quaternion.Euler(Operacion);
    }

    public void CambioDeAnguloMas90()
    {
        Vector3 Operacion = AnguloActual.eulerAngles + Sumar90.eulerAngles;
        AnguloActual = Quaternion.Euler(Operacion);
    }

    public void CambioDeAnguloMenos90()
    {
        Vector3 Operacion = AnguloActual.eulerAngles + Restar90.eulerAngles;
        AnguloActual = Quaternion.Euler(Operacion);
    }

    IEnumerator Rotacion(float duracion)
    {
        rotando = true;
        yield return new WaitForSecondsRealtime(TiempoEspera);

        Time.timeScale = 1.0f;
        rotando = false;
    }
}
