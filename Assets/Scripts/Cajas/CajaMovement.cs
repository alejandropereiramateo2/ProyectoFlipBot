using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Audio;

public class CajaMovement : MonoBehaviour
{

    [Header("Unity Events Movimiento")]

    public UnityEvent onBoxMove;
    public UnityEvent onBoxUnMove;

    [Header("Unity Events Audio")]

    public UnityEvent onPlaying;
    public UnityEvent onUnPlaying;

    [Header("Audio")]
    public AudioSource audios;

    public bool onBoxMoveZone { get; set; }
    public bool onAudioPlaying { get; set; }

    // Update is called once per frame
    void Update()
    {
        if (onBoxMoveZone)
        {
            onBoxMove.Invoke();
        }
        else
        {
            onBoxUnMove.Invoke();
        }

        if (onAudioPlaying)
        {
            if (!audios.isPlaying) {
                onPlaying.Invoke();
            }
            
        } else
        {
            onUnPlaying.Invoke();
        }

    }

}
