using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarGravedadCajas : MonoBehaviour
{

    [Header("RigidBody Caja")]
    public Rigidbody2D cajas;

    [Header("Duracion Gravedad")]
    public float duracionCambioGravedad;
    public float tiempoCambioGravedad;

    [Header("Variables Deseada")]
    public int gravedadDeseada;
    public int gravedadOriginal;

    public void cambioGravedadCajas()
    {
        tiempoCambioGravedad = 0;
        cajas.gravityScale = gravedadDeseada;
    }

    void Update()
    {
        tiempoCambioGravedad += Time.deltaTime;

        if (duracionCambioGravedad <= tiempoCambioGravedad)
        {
            cajas.gravityScale = gravedadOriginal;
        }
    }
}
