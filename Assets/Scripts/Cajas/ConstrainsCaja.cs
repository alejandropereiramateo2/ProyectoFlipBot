using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstrainsCaja : MonoBehaviour
{

    Rigidbody2D rigidbodyCajas;

    private void Start()
    {
        rigidbodyCajas = GetComponent<Rigidbody2D>();
    }

    public void FreezePositionX()
    {
        rigidbodyCajas.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
    }
    
    public void UnFreezePositionX()
    {
        rigidbodyCajas.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

}
