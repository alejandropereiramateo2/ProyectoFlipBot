using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CajaControler : MonoBehaviour
{

    [Header("Zonas de Activacion")]
    public UnityEvent onPlayerEnter;
    public UnityEvent onPlayerExit;

    [Header("Otras Variables")]
    public string tagPlayer;

    public Transform interactionZone;

    public bool autoActivate;
    public bool interact { get; set; }

    private bool contactoJugador;
    private Transform PlayerTransform;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagPlayer))
        {
            contactoJugador = true;
            PlayerTransform = collision.transform;
            onPlayerEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagPlayer))
        {
            contactoJugador = false;
            PlayerTransform = null;
            onPlayerExit.Invoke();
        }
    }





}


/*

public Rigidbody2D Caja;

    private CheckContactCajas checkCajas;

    public Vector3 vectorHit;

    // Start is called before the first frame update
    void Start()
    {
        Caja = GetComponent<Rigidbody2D>();
        checkCajas = GameObject.Find("Jugador").GetComponent<CheckContactCajas>();
    }

    // Update is called once per frame
    void Update()
    {
        Caja = checkCajas.hit.rigidbody;

        if (Input.GetButtonDown("Fire1") && checkCajas.canMove)
        {
            vectorHit = Caja.transform.position;

            GameObject box = Caja.gameObject;

            box.transform.position = new(vectorHit.x+1, vectorHit.y, vectorHit.z);

            checkCajas.canMove = false;
        } 
    }

*/
