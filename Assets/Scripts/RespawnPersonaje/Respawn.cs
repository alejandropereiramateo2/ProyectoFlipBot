using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Respawn : MonoBehaviour
{

    [Header("Variables Respawn")]

    public GameObject personaje1;
    public GameObject personaje2;
    public GameObject UIMuerte;

    public bool personajeMuerto { get; set; }

    [Header("Variables Reinicio")]
    public float contadorReinicio = 0f;
    public float tiempoReinicio = 0f;

    [Header("Evento Reinicio")]
    public UnityEvent onPulsarLaR;
    public UnityEvent onMuertePJs;

    // Start is called before the first frame update
    void Start()
    {
        personajeMuerto = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (personajeMuerto) {

            UIMuerte.SetActive(personajeMuerto);

            onMuertePJs.Invoke();
        }
        else
        {
            UIMuerte.SetActive(personajeMuerto);
        }

        if (!personaje1.activeSelf)
        {
            personajeMuerto = true;

            UIMuerte.SetActive(personajeMuerto);

            onMuertePJs.Invoke();
        }
        else
        {
            UIMuerte.SetActive(personajeMuerto);
        }

        if (!personaje2.activeSelf)
        {
            personajeMuerto = true;

            UIMuerte.SetActive(personajeMuerto);

            onMuertePJs.Invoke();
        }
        else
        {
            UIMuerte.SetActive(personajeMuerto);
        }

        if (Input.GetButton("Respawn"))
        {
            contadorReinicio += Time.deltaTime;

            if (contadorReinicio >= tiempoReinicio)
            {
                onPulsarLaR.Invoke();
            } 
        } else
        {
            contadorReinicio = 0;
        }

    }
}
