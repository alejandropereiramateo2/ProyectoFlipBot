using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReiniciarSala : MonoBehaviour
{

    [Header("Eventos")]
    public UnityEvent onKeyPress;

    public bool reiniciarLaSala { get; set; }

    // Update is called once per frame
    void Update()
    {
        if(reiniciarLaSala)
        {
            onKeyPress.Invoke();
        } 
    }



}
