using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivarBoton : MonoBehaviour
{

    [Header("Unity Events")]
    public UnityEvent onButtonPress;
    public UnityEvent onButtonUnPress;

    public bool onButtonPressZone { get; set; }

    // Update is called once per frame
    void Update()
    {

        if (onButtonPressZone)
        {
            onButtonPress.Invoke();

        } else
        {
            onButtonUnPress.Invoke();
        }
    }

}
