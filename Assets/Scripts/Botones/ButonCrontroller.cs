using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class ButonCrontroller : MonoBehaviour
{

    [Header("Zonas de Activacion")]
    public UnityEvent onBoxEnter;
    public UnityEvent onBoxExit;

    [Header("Otras Variables")]
    public string tagCajas;
    public string tagPlayer;

    public Transform interactionZone;

    public bool autoActivate;
    public bool interact { get; set; }

    private bool contactoCajaBoton;
    private Transform boxTransform;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagCajas))
        {
            contactoCajaBoton = true;
            boxTransform = collision.transform;
            onBoxEnter.Invoke();
        }

        if (collision.gameObject.tag.Equals(tagPlayer))
        {
            contactoCajaBoton = true;
            boxTransform = collision.transform;
            onBoxEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagCajas))
        {
            contactoCajaBoton = false;
            boxTransform = null;
            onBoxExit.Invoke();
        }

        if (collision.gameObject.tag.Equals(tagPlayer))
        {
            contactoCajaBoton = false;
            boxTransform = null;
            onBoxExit.Invoke();
        }
    }
}
