using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumeroDeGiros : MonoBehaviour
{

    public int numGiros;

    public TextMeshProUGUI numeroGiros;

    public void cambiarGiros()
    {
        numeroGiros.text = numGiros.ToString();
    }

    public void actualizarGiros()
    {
        numGiros += 1;
    }

}
