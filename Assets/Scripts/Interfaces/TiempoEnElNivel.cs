using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TiempoEnElNivel : MonoBehaviour
{
    public float tiempo;

    public TextMeshProUGUI textTiempo;

    // Update is called once per frame
    void Update()
    {
        tiempo += Time.deltaTime;

        actualizarTiempo();
    }

    public void actualizarTiempo()
    {
        textTiempo.text = tiempo.ToString("F0") + " S";
    }

}
