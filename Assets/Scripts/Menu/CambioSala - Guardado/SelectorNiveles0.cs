using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class SelectorNiveles0 : MonoBehaviour
{
    public string nombreSala;

    [Header("Guardado Nivel")]
    public string nombreVariableGuardado;
    public int numeroSalaCompletada;

    [Header("Eventos Cambio Sala")]
    public UnityEvent onSceneLoad;

    public bool botonPulsado { get; set; }

    public void SceneLoad()
    {
        if(botonPulsado)
        {
            onSceneLoad.Invoke();
        }
    }

    public void CambiarSala()
    {
        PlayerPrefs.SetInt(nombreVariableGuardado, numeroSalaCompletada);

        SceneManager.LoadScene(nombreSala);
    }

}
