using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class HabilitarBotonesSelecionNivel : MonoBehaviour
{

    [Header("Variables Guardado")]
    public string nombreVariableGuardado;
    public int numeroSalaCompletada;

    [Header("Boton")]
    public Button botonSala;

    private void OnEnable()
    {
        if (PlayerPrefs.GetInt(nombreVariableGuardado) >= numeroSalaCompletada)
        {
            botonSala.interactable = true;
        } else
        {
            botonSala.interactable = false;
        }
    }

    private void Update()
    {
        if (PlayerPrefs.GetInt(nombreVariableGuardado) >= numeroSalaCompletada)
        {
            botonSala.interactable = true;
        }
        else
        {
            botonSala.interactable = false;
        }
    }

}
