using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Botoneria : MonoBehaviour
{
    [Header("Eventos Cambio Sala")]
    public UnityEvent onConvinacionCorrecta;

    public bool botonPulsado1 { get; set; }
    public bool botonPulsado2 { get; set; }
    public bool botonPulsado3 { get; set; }

    private void Update()
    {
        if (botonPulsado1 && botonPulsado2 && botonPulsado3)
        {
            onConvinacionCorrecta.Invoke();
        }
    }

}
