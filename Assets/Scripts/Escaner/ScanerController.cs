using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScanerController : MonoBehaviour
{

    [Header("Zonas de Activacion")]
    public UnityEvent onPlayerEnter;
    public UnityEvent onPlayerExit;

    [Header("Otras Variables")]
    public string tagColision;

    public Transform interactionZone;

    public bool autoActivate;
    public bool interact { get; set; }

    private bool contactoConEscaner;
    private Transform objectTransform;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagColision))
        {
            contactoConEscaner = true;
            objectTransform = collision.transform;
            onPlayerEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagColision))
        {
            contactoConEscaner = false;
            objectTransform = null;
            onPlayerExit.Invoke();
        }
    }
}
