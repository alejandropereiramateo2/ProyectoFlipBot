using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MuerteValla : MonoBehaviour
{
    [Header("Variables")]
    public GameObject pj;
    public string tagPJ;

    [Header("Evento Victoria")]
    public UnityEvent onColision;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagPJ))
        {
            onColision.Invoke();
        }
    }
}
