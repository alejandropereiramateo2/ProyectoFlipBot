using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class MenuPausa : MonoBehaviour
{
    public bool juegoPausa;
    public GameObject MenuPausaUI;

    [Header("Sala Carga")]
    public string nombreSala;

    [Header("Eventos Pausa")]
    public UnityEvent onPausa;
    public UnityEvent onResume;


    public void JuegoPausa(bool pausado)
    {
        juegoPausa = pausado;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Escape"))
        {
            if (juegoPausa)
            {
                onResume.Invoke();
            } else
            {
                onPausa.Invoke();
            }
        }
    }

    public void Resume()
    {
        MenuPausaUI.SetActive(false);
        Time.timeScale = 1f;
        juegoPausa = false;
    }

    public void Pausar()
    {
        MenuPausaUI.SetActive(true);
        Time.timeScale = 0f;
        juegoPausa = true;
    }

    public void SceneLoad()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(nombreSala);
    }

}
