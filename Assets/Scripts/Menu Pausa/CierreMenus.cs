using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CierreMenus : MonoBehaviour
{
    
    public GameObject MenuPausaUI;

    void Update()
    {
        if (Input.GetButtonDown("Escape"))
        {
            MenuPausaUI.SetActive(false);
        }
    }
}
