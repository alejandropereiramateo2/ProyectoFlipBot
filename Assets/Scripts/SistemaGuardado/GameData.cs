using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{

    public int nivelesCompletados;

    public GameData()
    {
        this.nivelesCompletados = 0;    
    }
}
