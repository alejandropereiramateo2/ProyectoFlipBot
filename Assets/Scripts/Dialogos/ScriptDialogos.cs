using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class ScriptDialogos : MonoBehaviour
{

    [Header("Variables Dialogo")]
    public TextMeshProUGUI textoDialogo;
    public string[] lines;
    public float velocidadTexto = 0.1f;

    public float tiempoLectura = 0f;

    public int index;
    public int indexFinal;

    [Header("Evento Texto")]
    public UnityEvent onDialogoEnd;
    public UnityEvent onTextEnd;

    public void EmpezarDialogo()
    {
        StartCoroutine(EscribirLinea());
    }
    
    IEnumerator EscribirLinea()
    {
        StartCoroutine(TiempoDeLectura());

        foreach (char letras in lines[index].ToCharArray())
        {
            textoDialogo.text += letras;

            yield return new WaitForSeconds(velocidadTexto);
        }

    }

    private void Update()
    {

        if (index == indexFinal)
        {
            onTextEnd.Invoke();
        }
    }

    public void AumentarIndex()
    {
        if (index < indexFinal)
        {
            index += 1;
        } 
        
    }

    IEnumerator TiempoDeLectura()
    {
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        yield return new WaitForSecondsRealtime(tiempoLectura);

        onDialogoEnd.Invoke();

        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }

    //private void Update()
    //{
    //    if(index == indexFinal)
    //    {
    //        onTextEnd.Invoke();
    //    }
    //}

}
