using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class trigerDialogos : MonoBehaviour
{

    [Header("Zonas de Activacion")]
    public UnityEvent onZoneDialogoEnter;
    public UnityEvent onZoneDialogoExit;

    [Header("Otras Variables")]
    public string tagJugador;

    public Transform interactionZone;

    public bool autoActivate;
    public bool interact { get; set; }

    private bool contactoJugador;
    private Transform jugadorTransform;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagJugador))
        {
            contactoJugador = true;
            jugadorTransform = collision.transform;
            onZoneDialogoEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagJugador))
        {
            contactoJugador = false;
            jugadorTransform = null;
            onZoneDialogoExit.Invoke();
        }
    }
}
