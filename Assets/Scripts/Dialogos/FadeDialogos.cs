using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FadeDialogos : MonoBehaviour
{
    [Header("Variables Fade")]
    public CanvasGroup canvasFade;

    public float TimeToFade = 0.1f;

    public bool fadeIn { get; set; }
    public bool fadeOut { get; set; }


    [Header("Eventos Fade")]
    public UnityEvent onFadeIn;
    public UnityEvent onFadeOut;
    public UnityEvent onFadeEnd;

    private void Update()
    {
        if (fadeIn)
        {
            onFadeIn.Invoke();
        }

        if (fadeOut)
        {
            onFadeOut.Invoke();
        }
    }

    public void StartFadeOut()
    {
        if (canvasFade.alpha < 1)
        {
            canvasFade.alpha += TimeToFade * Time.deltaTime;

        } else
        {
            onFadeEnd.Invoke();
        }
    }

    public void StartFadeIn()
    {

        if (canvasFade.alpha >= 0)
        {
            canvasFade.alpha -= TimeToFade * Time.deltaTime;

        } 

        if (canvasFade.alpha <= 0)
        {
            onFadeEnd.Invoke();
        }
    }

}
