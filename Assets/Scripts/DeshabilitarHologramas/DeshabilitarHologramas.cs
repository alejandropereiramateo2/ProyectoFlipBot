using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeshabilitarHologramas : MonoBehaviour
{

    [Header("Obeto a Deshabilitar")]
    public GameObject objeto;

    [Header("Duracion Deshabilitado")]
    public float duracionTotalDeshabilitado;
    public float tiempoDeshabilitado;

    [Header("Eventos Deshabilitado")]
    public UnityEvent onHabilitar;
    public UnityEvent onDeshabilitar;

    // Update is called once per frame
    void Update()
    {
        tiempoDeshabilitado += Time.deltaTime;

        if (duracionTotalDeshabilitado <= tiempoDeshabilitado)
        {
            onHabilitar.Invoke();
        }
    }

    public void DeshabilitarObjeto()
    {
        tiempoDeshabilitado = 0f;
        onDeshabilitar.Invoke();
    }

}
