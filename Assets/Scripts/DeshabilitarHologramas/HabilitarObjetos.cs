using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HabilitarObjetos : MonoBehaviour
{
    [Header("Obeto a Habilitar")]
    public GameObject objeto;

    [Header("Duracion Habilitado")]
    public float duracionTotalHabilitado;
    public float tiempoHabilitado;

    [Header("Eventos Habilitado")]
    public UnityEvent onHabilitar;
    public UnityEvent onDeshabilitar;

    // Update is called once per frame
    void Update()
    {
        tiempoHabilitado += Time.deltaTime;

        if (duracionTotalHabilitado <= tiempoHabilitado)
        {
            onDeshabilitar.Invoke();
        }
    }

    public void HabilitarObjeto()
    {
        tiempoHabilitado = 0f;
        onHabilitar.Invoke();
    }
}
