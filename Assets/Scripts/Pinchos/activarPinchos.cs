using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class activarPinchos : MonoBehaviour
{
    [Header("Unity Events")]
    public UnityEvent onPinchosContact;
    public UnityEvent onPinchosUnContact;

    public bool onPinchosContactZone { get; set; }

    // Update is called once per frame
    void Update()
    {

        if (onPinchosContactZone)
        {
            onPinchosContact.Invoke();

        }
        else
        {
            onPinchosUnContact.Invoke();
        }
    }
}
