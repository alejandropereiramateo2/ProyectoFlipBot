using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ElectroPJ : MonoBehaviour
{
    [Header("Variables")]
    public GameObject pj;
    public Rigidbody2D pjRigid;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void freezePosition()
    {
        pjRigid.constraints = RigidbodyConstraints2D.FreezePosition;
    }

}
