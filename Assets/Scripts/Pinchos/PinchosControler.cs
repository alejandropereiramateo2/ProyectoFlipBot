using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PinchosControler : MonoBehaviour
{
    [Header("Zonas de Activacion")]
    public UnityEvent onPinchosEnter;
    public UnityEvent onPinchosExit;
    public UnityEvent onPlayerDead;

    [Header("Otras Variables")]
    public string tagCajas;
    public string tagPlayer;

    public Transform interactionZone;

    public bool autoActivate;
    public bool interact { get; set; }

    private bool contactoConObjeto;
    private Transform objetoTransform;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagCajas))
        {
            contactoConObjeto = true;
            objetoTransform = collision.transform;
            onPinchosEnter.Invoke();
        }

        if (collision.gameObject.tag.Equals(tagPlayer))
        {
            contactoConObjeto = true;
            objetoTransform = collision.transform;
            onPlayerDead.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagCajas))
        {
            contactoConObjeto = false;
            objetoTransform = null;
            onPinchosExit.Invoke();
        }
    }
}
