using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MuerteSofocado : MonoBehaviour
{
    public bool Sofocado { get; set; }

    [Header("Eventos")]
    public UnityEvent onMuerteSofocado;

    private void Update()
    {
        if (Sofocado)
        {
            onMuerteSofocado.Invoke();
        }
    }
}
