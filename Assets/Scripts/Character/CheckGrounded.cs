using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckGrounded : MonoBehaviour
{

    [Header("Jugador")]
    Rigidbody2D jugador;

    [Header("Variables Rayos")]
    public Transform posicionInicialIzq;
    public Transform posicionFinalIzq;

    public Transform posicionInicialMedio;
    public Transform posicionFinalMedio;

    public Transform posicionInicialDer;
    public Transform posicionFinalDer;

    [Header("Variables Layers")]
    public LayerMask layerIzq;
    public LayerMask layerMedio;
    public LayerMask layerDer;

    [HideInInspector]
    RaycastHit2D rayoIzq;

    [HideInInspector]
    RaycastHit2D rayoMedio;

    [HideInInspector]
    RaycastHit2D rayoDer;

    [Header("Eventos Contacto")]
    public UnityEvent onContact;
    public UnityEvent onUnContact;

    [Header("Game Object Jugador")]
    public GameObject jugadoGO;

    public bool TocandoSuelo { get; set; }

    public bool TocandoCaja { get; set; }

    public bool TocandoBoton { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        jugador = jugadoGO.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {

        rayoIzq = Physics2D.Linecast(posicionInicialIzq.position, posicionFinalIzq.position, layerIzq.value);
        rayoMedio = Physics2D.Linecast(posicionInicialMedio.position, posicionFinalMedio.position, layerMedio.value);
        rayoDer = Physics2D.Linecast(posicionInicialDer.position, posicionFinalDer.position, layerDer.value);


        if (rayoIzq.collider != null)
        {
            TocandoSuelo = true;
            TocandoCaja = true;
            TocandoBoton = true;

            onContact.Invoke();
        }

        if (rayoMedio.collider != null)
        {

            TocandoSuelo = true;
            TocandoCaja = true;
            TocandoBoton = true;

            onContact.Invoke();
        }

        if (rayoDer.collider != null)
        {
            TocandoSuelo = true;
            TocandoCaja = true;
            TocandoBoton = true;

            onContact.Invoke();
        }

        if(rayoIzq.collider == null && rayoMedio.collider == null && rayoDer.collider == null)
        {
            TocandoSuelo = false;
            TocandoCaja = false;
            TocandoBoton = false;

            onUnContact.Invoke();
        }

    }

}
