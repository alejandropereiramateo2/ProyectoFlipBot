using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControladorMuerte : MonoBehaviour
{
    [Header("Evento Texto")]
    public UnityEvent onAplastado;

    [Header("Otras Variables")]
    public string tagPlayer;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagPlayer))
        {
            onAplastado.Invoke();
        }
        
    }
}
