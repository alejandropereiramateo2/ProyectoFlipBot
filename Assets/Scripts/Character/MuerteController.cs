using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MuerteController : MonoBehaviour
{
    [Header("Variables Rayos")]
    public Transform posicionInicialIzqCaja;
    public Transform posicionFinalIzqCaja;

    public Transform posicionInicialMedioCaja;
    public Transform posicionFinalMedioCaja;

    public Transform posicionInicialDerCaja;
    public Transform posicionFinalDerCaja;

    public Transform posicionInicialIzqSuelo;
    public Transform posicionFinalIzqSuelo;

    public Transform posicionInicialMedioSuelo;
    public Transform posicionFinalMedioSuelo;

    public Transform posicionInicialDerSuelo;
    public Transform posicionFinalDerSuelo;

    [Header("Variables Layers")]
    public LayerMask layerCaja;
    public LayerMask layerSuelo;

    [Header("Eventos Contacto")]
    public UnityEvent onContact;

    [HideInInspector]
    RaycastHit2D rayoIzqCaja;

    [HideInInspector]
    RaycastHit2D rayoMedioCaja;

    [HideInInspector]
    RaycastHit2D rayoDerCaja;

    [HideInInspector]
    RaycastHit2D rayoIzqSuelo;

    [HideInInspector]
    RaycastHit2D rayoMedioSuelo;

    [HideInInspector]
    RaycastHit2D rayoDerSuelo;

    [Header("Variables Bools")]
    public bool tocandoCaja;
    public bool tocandoSuelo;

    private void FixedUpdate()
    {

        rayoIzqCaja = Physics2D.Linecast(posicionInicialIzqCaja.position, posicionFinalIzqCaja.position, layerCaja.value);
        rayoMedioCaja = Physics2D.Linecast(posicionInicialMedioCaja.position, posicionFinalMedioCaja.position, layerCaja.value);
        rayoDerCaja = Physics2D.Linecast(posicionInicialDerCaja.position, posicionFinalDerCaja.position, layerCaja.value);

        rayoIzqSuelo = Physics2D.Linecast(posicionInicialIzqSuelo.position, posicionFinalIzqSuelo.position, layerSuelo.value);
        rayoMedioSuelo = Physics2D.Linecast(posicionInicialMedioSuelo.position, posicionFinalMedioSuelo.position, layerSuelo.value);
        rayoDerSuelo = Physics2D.Linecast(posicionInicialDerSuelo.position, posicionFinalDerSuelo.position, layerSuelo.value);

        if (rayoIzqCaja.collider != null || rayoMedioCaja.collider != null || rayoDerCaja.collider != null)
        {
            tocandoCaja = true;
        } else
        {
            tocandoCaja = false;
        }

        if (rayoIzqSuelo.collider != null || rayoMedioSuelo.collider != null || rayoDerSuelo.collider != null)
        {
            tocandoSuelo = true;
        } else
        {
            tocandoSuelo = false;
        }

        if(tocandoCaja && tocandoSuelo)
        {
            onContact.Invoke();
        }

    }
}
