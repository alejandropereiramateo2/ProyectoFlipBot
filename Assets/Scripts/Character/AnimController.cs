using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimController : MonoBehaviour
{
    [Header("Variables")]
    public GameObject pj;
    public float time;

    public bool caida{ get; set; }
    public bool salto{ get; set; }

    [Header("Animator")]
    public Animator animator;

    public CharacterController characterController;

    [Header("Evento Animaciones")]
    public UnityEvent onVictoria;
    public UnityEvent onPinchos;
    public UnityEvent onReinicio;
    public UnityEvent onCaida;

    [Header("Eventos Update")]
    public UnityEvent onPulsarLaR;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        animator.SetFloat("Time", time);
        animator.SetBool("Caida", caida);
        animator.SetBool("Salto", salto);
        animator.SetFloat("Horizontal", Mathf.Abs(characterController.Horizontal));

        if (characterController.Horizontal != 0)
        {
            time = 0;
        }

        if (Input.GetButtonDown("Respawn"))
        {
            onPulsarLaR.Invoke();
        }

    }

    public void SiguienteNivel()
    {
        onVictoria.Invoke();
    }


    public void Electro()
    {
        onPinchos.Invoke();
    }

    public void Reinicio()
    {
        onReinicio.Invoke();
    }

    public void Caida()
    {
        onCaida.Invoke();
    }

    public void Salto()
    {

    }

}
