using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterController : MonoBehaviour
{

    Rigidbody2D jugador;

    public float Velocidad = 5f;
    public float VelocidadSalto = 1f;
    public float Horizontal = 0f;

    private CheckGrounded checkSuelo;

    [Header("Unity Events")]
    public UnityEvent onJump;

    public bool onLanding { get; set; }
    public bool onSalto { get; set; }

    [Header("Game Object Jugador")]
    public GameObject jugadorGO;
    public Vector2 mirarDer;
    public Vector2 mirarIzq;

    // Start is called before the first frame update
    void Start()
    {
        jugador = gameObject.GetComponent<Rigidbody2D>();
        checkSuelo = jugadorGO.GetComponent<CheckGrounded>();
    }

    // Update is called once per frame
    void Update()
    {

        Horizontal = Input.GetAxisRaw("Horizontal");
        jugador.velocity = new Vector2(Horizontal * Velocidad, jugador.velocity.y);

        if (Input.GetButtonDown("Jump") && checkSuelo.TocandoSuelo)
        {
            jugador.velocity = Vector2.up * VelocidadSalto;
            checkSuelo.TocandoSuelo = false;


            onJump.Invoke();
        } 
        
        if (Input.GetButtonDown("Jump") && checkSuelo.TocandoCaja)
        {
            jugador.velocity = Vector2.up * VelocidadSalto;
            checkSuelo.TocandoCaja = false;

            onJump.Invoke();
        }

        if (Input.GetButtonDown("Jump") && checkSuelo.TocandoBoton)
        {
            jugador.velocity = Vector2.up * VelocidadSalto;
            checkSuelo.TocandoBoton = false;

            onJump.Invoke();
        }

        if (Horizontal < 0)
        {
            jugadorGO.transform.localScale = mirarIzq;
        }
        else if (Horizontal > 0)
        {
            jugadorGO.transform.localScale = mirarDer;
        }
    }
}
