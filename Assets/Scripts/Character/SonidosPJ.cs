using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SonidosPJ : MonoBehaviour
{

    [Header("Unity Events")]
    public UnityEvent onSonidoAndar;
    public UnityEvent onSonidoEmpujarCaja;
    public UnityEvent onSonidoSalto;
    public UnityEvent onSonidoVictoria;

    public void SonidoAndar()
    {
        onSonidoAndar.Invoke();
    }

    public void SonidoEmpujarCaja()
    {
        onSonidoEmpujarCaja.Invoke();

    }

    public void SonidoSalto()
    {
        onSonidoSalto.Invoke();
    }

    public void SonidoVictoria()
    {
        onSonidoVictoria.Invoke();
    }

}
