using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MuertePersonaje : MonoBehaviour
{

    [Header("Variables Rayos")]
    public Transform posicionInicialIzq;
    public Transform posicionFinalIzq;

    public Transform posicionInicialMedio;
    public Transform posicionFinalMedio;

    public Transform posicionInicialDer;
    public Transform posicionFinalDer;

    [Header("Variables Layers")]
    public LayerMask layerCajas;

    [HideInInspector]
    RaycastHit2D rayoIzq;

    [HideInInspector]
    RaycastHit2D rayoMedio;

    [HideInInspector]
    RaycastHit2D rayoDer;

    [Header("Eventos Contacto")]
    public UnityEvent onMuerte;

      public bool onAir { get; set; }

    Rigidbody2D jugador;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rayoIzq = Physics2D.Linecast(posicionInicialIzq.position, posicionFinalIzq.position, layerCajas.value);
        rayoMedio = Physics2D.Linecast(posicionInicialMedio.position, posicionFinalMedio.position, layerCajas.value);
        rayoDer = Physics2D.Linecast(posicionInicialDer.position, posicionFinalDer.position, layerCajas.value);

        if (rayoIzq.collider != null || rayoMedio.collider != null || rayoDer.collider != null)
        {
            onMuerte.Invoke();
        }

    }
}
