using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRotation : MonoBehaviour
{
    private Quaternion Angulo0 = Quaternion.Euler(0, 0, 0);

    public Quaternion AnguloActual;
    public float VelocidadRotacion = 0.2f;

    public bool rotando;
    public float duracionRotacion;

    private bool OverlapCircle = false;

    private CheckGrounded checkSuelo;

    [Header("jugador")]
    public GameObject jugador;

    // Start is called before the first frame update
    void Start()
    {
        AnguloActual = Angulo0;

        checkSuelo = jugador.GetComponent<CheckGrounded>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("FlechaUp")) //Sumar 180 
        {
            if (rotando)
            {
                return;
            }
            else
            {
                if (OverlapCircle)
                {
                    RotacionPersonajeA0();
                }
            }
        }

        if (Input.GetButtonDown("FlechaRight")) //Sumar 90 (Girar Der)
        {
            if (rotando)
            {
                return;

            }
            else
            {
                if (OverlapCircle)
                {
                    RotacionPersonajeA0();
                }
            }
        }

        if (Input.GetButtonDown("FlechaLeft")) //Restar 90 (Girar Izq)
        {
            if (rotando)
            {
                return;

            }
            else
            {
                if (OverlapCircle)
                {
                    RotacionPersonajeA0();

                }
            }
        }

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, AnguloActual, VelocidadRotacion);

    }

    private void FixedUpdate()
    {
        if (checkSuelo.TocandoSuelo == true)
        {
            OverlapCircle = true;
        }
        else
        {
            OverlapCircle = false;
        }
    }

    public void RotacionPersonajeA0()
    {
        Vector3 vectorAux = Angulo0.eulerAngles;
        AnguloActual = Quaternion.Euler(vectorAux);
    }

}