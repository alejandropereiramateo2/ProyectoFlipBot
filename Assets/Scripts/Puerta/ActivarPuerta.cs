using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivarPuerta : MonoBehaviour
{

    [Header("Unity Events")]
    public UnityEvent onDoorEnter;

    public bool onDoorZone { get; set; }


    // Update is called once per frame
    void Update()
    {
        if (onDoorZone)
        {
            onDoorEnter.Invoke();
        }
    }
}
