using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class ControladorPuerta : MonoBehaviour
{

    [Header("Zonas de Activacion")]
    public UnityEvent onDoorEnter;
    public UnityEvent onDoorExit;

    [Header("Otras Variables")]
    public string tagPuerta;

    public Transform interactionZone;

    public bool autoActivate;
    public bool interact { get; set; }

    private bool contactoPuerta;
    private Transform PuertaTransform;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagPuerta))
        {
            contactoPuerta = true;
            PuertaTransform = collision.transform;
            onDoorEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(tagPuerta))
        {
            contactoPuerta = false;
            PuertaTransform = null;
            onDoorExit.Invoke();
        }
    }
}
