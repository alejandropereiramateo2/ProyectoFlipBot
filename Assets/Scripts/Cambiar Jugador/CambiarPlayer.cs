using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CambiarPlayer : MonoBehaviour
{
    [Header("Eventos Cambio Sala")]
    public UnityEvent cambiarPJ1;
    public UnityEvent cambiarPJ2;

    public void CambiarPersonajeP1()
    {
        cambiarPJ1.Invoke();
    }

    public void CambiarPersonajeP2()
    {
        cambiarPJ2.Invoke();
    }
}
