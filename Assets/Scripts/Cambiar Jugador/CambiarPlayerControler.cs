using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CambiarPlayerControler : MonoBehaviour
{
    public bool personajeCambiado { get; set; }

    [Header("Eventos Cambio Sala")]
    public UnityEvent onPJCambio1;
    public UnityEvent onPJCambio2;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cambio PJ"))
        {
            if (personajeCambiado)
            {
                onPJCambio1.Invoke();
                Debug.Log(personajeCambiado.ToString());
            } else
            {
                onPJCambio2.Invoke();
            }
            
        }
    }
}
